package ru.test.btlab.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.test.btlab.model.FormProduct;
import ru.test.btlab.model.Product;
import ru.test.btlab.service.CheckerConditionProduct;
import ru.test.btlab.service.ProductService;
import ru.test.btlab.service.ProductServiceImpl;

import java.util.List;

@Controller
public class ProductController {

    private ProductService productService;

    @RequestMapping(value = "/main",method = RequestMethod.GET)
    public String main(Model model){

        productService = new ProductServiceImpl();
        List<Product> products = productService.findAll();

        model.addAttribute("products",products);

        return "main";
    }

    @RequestMapping(value = "/add_product",method = RequestMethod.POST)
    public String add_product(){

    }

    @RequestMapping(value = { "/add_product" }, method = RequestMethod.POST)
    public String savePerson(Model model, @ModelAttribute("product") FormProduct productF) {

        CheckerConditionProduct check = new CheckerConditionProduct(productF);
        if(check.getMessageChecker().equals("ok")){

            Product product = new Product();
            productService = new ProductServiceImpl();

            product.transformation(productF);

            productService.save(product);

            return "redirect:/main";
        }else{
            model.addAttribute("errorMessage", check.getMessageChecker());
            return "addPerson";
        }
    }

    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    public String edit_product(Model model,@ModelAttribute("product"),){
        CheckerConditionProduct check = new CheckerConditionProduct(productF);
        if(check.getMessageChecker().equals("ok")){

        }else{
            model.addAttribute("errorMessage", check.getMessageChecker());
            return "edit";
        }
    }

}
