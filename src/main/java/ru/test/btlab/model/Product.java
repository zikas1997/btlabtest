package ru.test.btlab.model;

import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name="Product")
public class Product {

    //----------------------------------------------------

    @Id
    @Column(name="id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    @Column(name="name")
    private String name;
    @Column(name="description")
    private String description;
    @Column(name="create_date")
        private Date dateCreate;
    @Column(name="place_storage")
    private int placeStorage;
    @Column(name="reserved")
    private int reserved;
    private boolean reservedCheck;

    //----------------------------------------------------

    public Product() {
    }

    //----------------------------------------------------

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getPlaceStorage() {
        return placeStorage;
    }

    public void setPlaceStorage(int placeStorage) {
        this.placeStorage = placeStorage;
    }

    public boolean isReservedCheck() {
        return reservedCheck;
    }

    public int getReserved() {
        return reserved;
    }

    public void setReserved(int reserved) {
        if(reserved==1){
          setReservedCheck(true);
        }
        if(reserved==0){
            setReservedCheck(false);
        }
        this.reserved = reserved;
    }

    public void setReservedCheck(boolean reservedCheck) {
        if(reservedCheck ==true){
            setReserved(1);
        }else {
            setReserved(0);
        }
        this.reservedCheck = reservedCheck;
    }

    public void transformation(FormProduct formProduct){
        setId(formProduct.getId());
        setName(formProduct.getName());
        setDescription(formProduct.getDescription());
        setDateCreate(formProduct.getDateCreate());
        setPlaceStorage(Integer.parseInt(formProduct.getPlaceStorage()));
        setReserved(formProduct.getReserved());
    }
}
