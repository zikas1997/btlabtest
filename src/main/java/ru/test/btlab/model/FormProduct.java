package ru.test.btlab.model;

import java.sql.Date;

public class FormProduct {

    //----------------------------------------------------

    private long id;
    private String name;
    private String description;
    private Date dateCreate;
    private String placeStorage;
    private int reserved;
    private boolean reservedCheck;

    //----------------------------------------------------

    public FormProduct() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getPlaceStorage() {
        return placeStorage;
    }

    public void setPlaceStorage(String placeStorage) {
        this.placeStorage = placeStorage;
    }

    public int getReserved() {
        return reserved;
    }

    public boolean isReservedCheck() {
        return reservedCheck;
    }

    public void setReserved(int reserved) {
        if(reserved==1){
            setReservedCheck(true);
        }
        if(reserved==0){
            setReservedCheck(false);
        }
        this.reserved = reserved;
    }

    public void setReservedCheck(boolean reservedCheck) {
        if(reservedCheck ==true){
            setReserved(1);
        }else {
            setReserved(0);
        }
        this.reservedCheck = reservedCheck;
    }
}
