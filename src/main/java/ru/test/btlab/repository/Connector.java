package ru.test.btlab.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class Connector {

    @Autowired
    private DriverManagerDataSource connection;

    //---------------------------------------------------------------------------------------------
    public Connector(){}
    //---------------------------------------------------------------------------------------------

    //---------------------------------------------------------------------------------------------
    protected Connection getConnection() {
        try {
            return connection.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    //---------------------------------------------------------------------------------------------
    protected void closeConnection(Connection connection) {
        try
        {
            if(connection != null) {
                connection.close();
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
    //---------------------------------------------------------------------------------------------
}
