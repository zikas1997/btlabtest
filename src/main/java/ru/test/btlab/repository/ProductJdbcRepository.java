package ru.test.btlab.repository;

import ru.test.btlab.model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductJdbcRepository extends Connector{

    //----------------------------------------------------------------------------------------------------------------

    private Connection connection;

    //----------------------------------------------------------------------------------------------------------------

    public ProductJdbcRepository() {

    }

    //----------------------------------------------------------------------------------------------------------------

    public void addProduct(Product product) {
        try {
            connection = getConnection();

            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO " +
                            "Product (id, name, description, create_date, place_storage, reserved)" +
                            " VALUES (?,?,?,?,?,?)");

            preparedStatement.setLong(1, product.getId());
            preparedStatement.setString(2, product.getName());
            preparedStatement.setString(3, product.getDescription());
            preparedStatement.setDate(4, product.getDateCreate());
            preparedStatement.setInt(5, product.getPlaceStorage());
            preparedStatement.setInt(6, product.getReserved());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

    public void addListProduct(List<Product> products) {
        try
        {
            connection = getConnection();

            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO " +
                            "Product (id, name, description, create_date, place_storage, reserved)" +
                            " VALUES (?,?,?,?,?,?)");

            for (Product product : products)
            {
                preparedStatement.setLong(1, product.getId());
                preparedStatement.setString(2, product.getName());
                preparedStatement.setString(3, product.getDescription());
                preparedStatement.setDate(4, product.getDateCreate());
                preparedStatement.setInt(5, product.getPlaceStorage());
                preparedStatement.setInt(6,product.getReserved());
                preparedStatement.addBatch();
            }

            preparedStatement.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

    public void deleteProductById(Long Id) {
        try {
            connection = getConnection();
            PreparedStatement preparedStatement =
                    connection.prepareStatement("DELETE Product WHERE Product.id = ? ");
            preparedStatement.setLong(1, Id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

    public void deleteProduct(Product product) {
        try {
            connection = getConnection();
            PreparedStatement preparedStatement =
                    connection.prepareStatement("DELETE Product WHERE Product.id = ? ");
            preparedStatement.setLong(1, product.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
    }

    public List<Product> findAllProduct()
    {
        List<Product> products = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM Product");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next())
                products.add(getProduct(resultSet));
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return products;
    }

    public Product findProductById(Long id) {
        Product product = new Product();
        try {
            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT * FROM Product " +
                    "WHERE id = ? ");
            preparedStatement.setLong(1, product.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            product = getProduct(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }
        return product;
    }

    public void updateProduct(Product product) {
        try {
            connection = getConnection();
            PreparedStatement preparedStatement =
                    connection.prepareStatement("UPDATE Product " +
                            "SET name = ?, description = ?, create_date = ?, place_storage = ?, reserved = ?" +
                            " WHERE id = ?");

            preparedStatement.setLong(6, product.getId());
            preparedStatement.setString(1, product.getName());
            preparedStatement.setString(2, product.getDescription());
            preparedStatement.setDate(3, product.getDateCreate());
            preparedStatement.setInt(4, product.getPlaceStorage());
            preparedStatement.setInt(5, product.getReserved());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            closeConnection(connection);
        }


    }
    protected Product getProduct(ResultSet resultSet) throws SQLException {

        Product product = new Product();

        product.setId(resultSet.getLong("id"));
        product.setName(resultSet.getString("name"));
        product.setDescription(resultSet.getString("description"));
        product.setDateCreate(resultSet.getDate("create_date"));
        product.setPlaceStorage(resultSet.getInt("place_storage"));
        product.setReserved(resultSet.getInt("reserved"));

        return product;
    }
}
