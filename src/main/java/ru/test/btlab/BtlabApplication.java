package ru.test.btlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BtlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(BtlabApplication.class, args);
	}
}
