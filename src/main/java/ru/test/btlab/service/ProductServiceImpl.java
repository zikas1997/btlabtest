package ru.test.btlab.service;

import ru.test.btlab.model.Product;
import ru.test.btlab.repository.ProductJdbcRepository;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    //-----------------------------------------------------------------------------------------------------------------

    private ProductJdbcRepository productRepository;

    //-----------------------------------------------------------------------------------------------------------------

    public ProductServiceImpl() {productRepository =  new ProductJdbcRepository();}

    //-----------------------------------------------------------------------------------------------------------------

    @Override
    public void save(Product product) {
        productRepository.addProduct(product);
    }

    @Override
    public void save(List<Product> products) {
        productRepository.addListProduct(products);
    }

    @Override
    public void delete(Long id) {
        productRepository.deleteProductById(id);
    }

    @Override
    public void delete(Product product) {
        productRepository.deleteProduct(product);
    }

    @Override
    public List<Product> findAll() {
        return productRepository.findAllProduct();
    }

    @Override
    public Product findById(Long id) {
        return productRepository.findProductById(id);
    }

    @Override
    public void edit(Product product) {
        productRepository.updateProduct(product);
    }
}
