package ru.test.btlab.service;

import ru.test.btlab.model.FormProduct;
import ru.test.btlab.model.Product;

import java.io.IOException;

public class CheckerConditionProduct {

    private FormProduct product;
    private String isProduct = "ok";

    public CheckerConditionProduct(FormProduct product) {
        this.product = product;
    }

    public String getMessageChecker(){

        try {
            Integer.parseInt(product.getPlaceStorage());
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
            isProduct="Номер ячейки должен быть числом";
        }
        if(product.getDateCreate()==null)
            isProduct="Дата не заполнена";
        if(product.getDescription().trim().length() == 0)
            isProduct="Введите описание товара";
        if(product.getName().trim().length() == 0)
            isProduct="Введите названия товара";
        return isProduct;
    }
}
