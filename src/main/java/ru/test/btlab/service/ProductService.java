package ru.test.btlab.service;

import ru.test.btlab.model.Product;

import java.util.List;

public interface ProductService {
    void save(Product product);
    void save(List<Product> products);
    void delete(Long id);
    void delete(Product product);
    List<Product> findAll();
    Product findById(Long id);
    void edit(Product product);
}
